# This repo is compriezed of various parallel programs.
These programs are at a beginner level and explore different methods for threading. They have all been tested on Fedora 17.

__Examples include:__

* pthreads
* OMP
* MPI

__TODO:__

* Include a darts example using fork()
* Try native C++ threading that is new in C++ 11