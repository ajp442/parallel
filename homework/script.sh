 #! /bin/bash
 
 #Clear the outputfile
 ''>output.csv
 file='../open.hosts'
 #for each processor number (1, 2, 4, 8, 16)
 for i in 1 2 4 8 16
 do
     #and for every problem size
     echo "number of processors: $i" | tee -a output.csv
     for j in 1024 2048 4096 8192 16384
     do
         x=$j
         x+=' '
         x+=$j
         #run the program with the number of processors using the
         #variable j as input to the program
         #tee will output the values to screen and to an output file
         echo -n "$j, " | tee -a output.csv
         mpiexec -np $i -hostfile $file ./mpi_mat_vect_mult <<<$x | tee -a output.csv
     done
 done
