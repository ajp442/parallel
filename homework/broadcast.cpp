#include <iostream>
#include <stdio.h>
#include <math.h>


using namespace std;

void send(int rank, int step, int comm_sz);
void recv(int rank, int step);


int main(int argc, char* argv[])
{
    int comm_sz = 16;
    double steps;
    int step;
    int two2theStep;
    int two2theStepMinus1;
    steps = ceil(log(comm_sz));
    cout << "step: " << step << endl;

    for (int rank = 0; rank < comm_sz; rank++)
    {
        for (step = (int)steps; step >= 0; step--)
        {
            two2theStep = (int)pow(2,step+1);
            two2theStepMinus1 = (int)pow(2,step);
            if (rank%two2theStep == 0)
            {
                //cout << "rank " << rank << " sending." << endl;
                send(rank, step, comm_sz);
            }
            else if (rank%two2theStepMinus1 == 0)
            {
                //cout << "rank " << rank << " recieving." << endl;
                recv(rank, step);
            }
        }
    }

    return 0;
}

void send(int rank, int step, int comm_sz)
{
    int val;
    val = rank + pow(2,step);
    if (val < comm_sz)
    {
        cout << rank << " sending to " << val << endl;
    }
}

void recv(int rank, int step)
{
    int val;
    val = rank - pow(2, step - 1);
    cout << rank << " recieving from " << val << endl;
}
