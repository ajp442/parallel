#include <iostream>
#include <mpi.h>

#define ARR_SIZE 9

using namespace std;

void prefixSums(int rank, int myNumber, int thread_count);

int main(int argc, char* argv[])
{
    int thread_count;//Number of processes.
    int rank;//Rank (ID) of each thread.
    int arr[ARR_SIZE] = {0, 1, 2, 5, 1, 2, 8, 4, 9};
    int prefix_sums[ARR_SIZE];
    int prefix_sum;


    
    //MPI_Init(&argc, &argv);
    MPI_Init(NULL, NULL);//Initiate.
    MPI_Comm_size(MPI_COMM_WORLD, &thread_count);//Get number of processes.
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);//Get the rank of the process.


        for (int i = 0; i < ARR_SIZE; i++)
        {
            prefix_sums[i] = 0;
        }

    if (rank == 0) 
    {


        cout << "Befor prefix sums" << endl;
        for (int i = 1; i < ARR_SIZE; i++)
        {
            cout << arr[i] << ", ";
        }
        cout << endl;

        for (int i = 1; i < thread_count; i++)
        {
            MPI_Recv(&prefix_sums[i], 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        cout << "After parallel prefix susm" << endl;
        for (int i = 1; i < ARR_SIZE; i++)
        {
            cout << prefix_sums[i] << ", ";
        }
        cout << endl;
    }

    else 
    {
        for (int i = 0; i < ARR_SIZE; i++)
        {
            prefix_sums[i] = 0;
        }
        //Calculate the number of tosses each thread does.
        prefixSums(rank, arr[rank], thread_count);
    }

    

    MPI_Finalize();

    return 0;

}


void prefixSums(int rank, int myNumber, int thread_count)
{
    int receiver;
    int sender;
    int partial_sum = myNumber;
    int prefix_sum = 0;
    int incomming = 0;

    cout << "my rank: " << rank << " my Number " << myNumber << endl;

    for (int comm_step = 1; comm_step<thread_count; comm_step++)
    {
        receiver = rank + comm_step;
        sender = rank - comm_step;

        if (receiver >= thread_count){
            receiver = -1;
        }


        if( receiver != -1)
        {
            MPI_Send(&partial_sum, 1, MPI_INT, receiver, 0, MPI_COMM_WORLD);
        }

        if(sender > 0)
        {
            MPI_Recv(&incomming, 1, MPI_INT, sender, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            partial_sum = partial_sum + incomming;
        }

    }

    prefix_sum = incomming;
    MPI_Send(&prefix_sum, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);

}
