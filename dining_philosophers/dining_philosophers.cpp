/*************************************************************************//**
 * @file
 *
 * @brief Classical dining philosopher problem.
 *
 * @mainpage Overview
 *
 * @section course_section Parallel Computing
 *
 * @author Andrew Pierson
 *
 * @date February 22, 2013
 *
 * @par Professor:
 *         Dr. Christer Karlsson
 *
 * @par Course:
 *         CSC 410 - M001 - 9:00am
 *
 * @par Location:
 *         McLaury
 *
 * @section program_section Program Information
 *
 * @details 
 * The problem is defined as follows: There are 5 philosophers sitting at a
 * round table. Between each adjacent pair of philosophers is a chopstick. In
 * other words, there are five chopsticks. Each philosopher does two things:
 * think and eat. The philosopher thinks for a while, and then stops thinking
 * and becomes hungry. When the philosopher becomes hungry, he/she cannot eat
 * until he/she owns the chopsticks to his/her left and right. When the
 * philosopher is done eating he/she puts down the chopsticks and begins
 * thinking again.
 *
 * 1) Deadlock Protocol
 * When a philosopher is hungry he/she looks to the left. If a chopstick is
 * available the philosopher will grab it. Next the philosopher looks to the
 * right, if that chopstick is available they will grab it. Once they have both
 * chopsticks they will eat until they are full then put down the chopsticks for
 * others to use. The deadlock occurs when every philosopher grabs one chopstick
 * and cannot eat because there is no way to get two chopsticks because every
 * philosopher is waiting for another philosopher to put down a chopstick.
 *
 * 2) Sharing Protocol
 * When a philosopher is hungry he/she looks to the left and to the right
 * to make sure both chopsticks are available before grabbing them.
 * If one or both chopsticks are unavailable the philosopher is polite and 
 * will wait for one to become available knowing that they will get their 
 * fair time quanta to eat.
 *
 *
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *       g++ dining_philosophers.cpp -lpthread -o dining_philosophers
 *
 * @par Usage:
 * $ dining_philosophers
 * 
 * DESCRIPTION
 * Runs a simulation for 5 seconds. The simulations consists of 5 
 * philosophers sitting in a circle. These philosophers do two
 * things: eat, and think.  
 *
 * At the end of the 5 seconds the philosophers will tell us how
 * hungry each of them got.
 *
 * There are two modes you can run the program in: deadlock or normal.
 * Deadlock mode allows the philosophers to grab one chopstick at a 
 * time which leads to an eventual deadlock. In normal mode philosophers
 * make sure they are able to acquire both chopsticks before taking them.
 * 
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 *
 *
 * @todo
 *
 * @par Modifications and Development Timeline:
 * @verbatim
 * Date          Modification
 * ------------  --------------------------------------------------------------
 * Feb 3, 2013   Program assigned
 * Feb 22, 2012  Started program and parsing arguments.
 * Feb 24, 2012  Wrote the deadlock version.
 * Feb 25, 2012  Wrote the non-deadlock version.
 * Feb 26, 2012  Combined the two versions and added comments.
 * Feb 28, 2012  Changed rand() to rand_r();
 * @endverbatim
 *
 *****************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <iostream>
#include <stdlib.h>
#include <iomanip>

#define NICE 100//The greater this is, the longer the philosophers will wait if other philosophers are hungry.
#define RUNTIME 5//How long the program runs for.

//****GLOBALS****
int thread_count = 5;//Number of philosophers.
bool thinkSession;//Flag indicating when their thinking session is over.
bool chopstick[5] = {true, true, true, true, true};//True if available.
pthread_mutex_t m_lock;//Mutex to prevent race conditions.
long int hungry[5] = {0,0,0,0,0};//Keeps track of how many times each philosopher went without food.

//****Function Prototypes****
void* thinkNEat(void* rank);
void* thinkNstarve(void* rank);
bool grabChopsticks(int rank);
bool grabLeftChopstick(int rank);
bool grabRightChopstick(int rank);
void releaseChopsticks(int rank);

int main(int argc, char* argv[])
{
    pthread_t*  threads;//The threads.
    int re;//Return value.
    int thread;//An iterator variable for joining.
    char response;//For getting user input.
    bool deadlock = false;//To set whether or not the philosophers should deadlock.

    //Output info about the program to the user.
    std::cout << "----------------------------------------------\n";
    std::cout << "          DINING PHILOSOPHERS\n";
    std::cout << "----------------------------------------------\n";
    std::cout << "Philosophers will dine for " << RUNTIME <<" seconds then print out how hungry they became while waiting for chopsticks.\n";
    std::cout << "Would you like them to deadlock [y/n] ";
    std::cin >> response;

    //Gather response from user.
    if (response == 'y' || response == 'Y')
    {
        deadlock = true;
    }
    else if (response == 'n' || response == 'N') {;}
    else{
        printf("Please enter 'y' or 'n'\n");
        return 1;
    }



    //Allocate memory for threads.
    threads = new (std::nothrow) pthread_t[thread_count];
    if (threads == NULL)
    {
        printf("failed to create threads \n");
        exit(1);
    }

    //Initialize stuff before running threads.
    thinkSession = true;

    //--------------------Create threads DEADLOCK VERSION----------------------
    if (deadlock){
        for(thread = 0; thread < thread_count; thread++){
            re = pthread_create(&threads[thread], NULL, thinkNstarve, new int(thread));
            if(re){
                printf("ERROR: return code from pthread_create() is %d\n", re);
                exit(-1);
            }
        }
    }
    //----------------Create threads FAIR VERSION------------------------------
    else{
        for(thread = 0; thread < thread_count; thread++){
            re = pthread_create(&threads[thread], NULL, thinkNEat, new int(thread));
            if(re){
                printf("ERROR: return code from pthread_create() is %d\n", re);
                exit(-1);
            }
        }
    }


    sleep(RUNTIME);//Let them think and eat for 5 seconds.
    thinkSession = false;//Signal to all threads that dinner is over.

    //Gather all the philosophers.
    for(thread = 0; thread < thread_count; thread++){
        re =pthread_join(threads[thread], NULL);
        if(re){
            printf("ERROR, return code from pthread_join() is %d\n", re);
            exit(1);
        }
    }


    //Display how hungry each philosopher was.
    for(int i = 0; i < thread_count; i++){
        std::cout << "Thread " << i << " was hungry for " << hungry[i] << " cycles \n";
    }

    //Now that we are done, do some cleanup.
    free(threads);
    pthread_mutex_destroy(&m_lock);
    return 0;
}




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: The non-deadlock version where the philosopher must
 * grab both chopsticks at the same time.
 *
 *
 * @param[in] rank - Integer of philosopher/thread.
 *
 * @returns NULL - When thinkSession is over.
 *****************************************************************************/
void* thinkNEat(void* rank)
{
    int* my_rank_ptr = reinterpret_cast<int*>(rank);//Id for the philosopher.
    int my_rank = *my_rank_ptr;//Cast the pointer to an int.
    int thinkTime;//Stores random time alloted to think.
    int eatTime;//Stores random time alloted to eat.
    int lphil = (my_rank + 1) % thread_count;//Philosopher to the left.
    int rphil = my_rank -1;//Philosopher to the right.
    int beNice = 0;//Iterator of how nice the philosopher should be.
    unsigned int seed;
    if (rphil < 0){ rphil = 4;}//Correcting rphil if it becomes negative.

    //Seed the random number generator.
    seed = (time(NULL)+ my_rank);
    printf("philosopher %d is at the table\n", my_rank);


    while(thinkSession)
    {

        thinkTime = 100 * ((rand_r(&seed) % 100) + 1);
        eatTime = 100 * ((rand_r(&seed) % 100) + 1);

        usleep(thinkTime);

        //If your fellow philosophers are more hungry than you are, then
        //be nice and give them a chance to pick up the chopsticks.
        if (hungry[my_rank] < hungry[rphil] || hungry[my_rank] < hungry[lphil])
            for (beNice = NICE; beNice > 0; beNice --)
                hungry[my_rank]++;

        //The philosopher is now hungry keep looking around
        //the table for some chopsticks to appear.
        while(grabChopsticks(my_rank)){
                hungry[my_rank]++;
        }

        printf("philosopher %d has both chopsticks\n", my_rank);
        usleep(eatTime);

        releaseChopsticks(my_rank);
    }

    return NULL;
}



/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Deadlock version where philosophers will first gab left
 * chopstick then right chopstick.
 *
 *
 * @param[in] rank - Integer of philosopher/thread.
 *
 * @returns NULL - When thinkSession is over.
 *****************************************************************************/
void* thinkNstarve(void* rank)
{
    int* my_rank_ptr = reinterpret_cast<int*>(rank);//Id for the philosopher.
    int my_rank = *my_rank_ptr;//Cast the pointer to an int.
    int thinkTime;//Stores random time alloted to think.
    int eatTime;//Stores random time alloted to eat.
    int lphil = (my_rank + 1)%5;//Philosopher to the left.
    int rphil = my_rank -1;//Philosopher to the right.
    int beNice = 0;//Iterator of how nice the philosopher should be.
    unsigned int seed;
    if (rphil < 0){ rphil = 4;}//Correcting rphil if it becomes negative.

    //Seed the random time generator with the current time.
    seed = (time(NULL) + my_rank);
    printf("philosopher %d is at the table\n", my_rank);
    while(thinkSession)
    {
        thinkTime = 100 * ((rand_r(&seed) % 100) + 1);
        eatTime = 100 * ((rand_r(&seed) % 100) + 1);

        usleep(thinkTime);

        //If your fellow philosophers are more hungry than you are, then
        //be nice and give them a chance to pick up the chopsticks.
        if (hungry[my_rank] < hungry[rphil] || hungry[my_rank] < hungry[lphil])
            for (beNice = NICE; beNice > 0; beNice--)//From experimentation I found this to be a good value.
                hungry[my_rank]++;

        //Grab Left chopstick first.
        while(grabLeftChopstick(my_rank)){
            hungry[my_rank]++;
        }
        //A little assistance for getting them to deadlock so we don't have to wait forever.
        usleep(5000);
        //Then grab Right chopstick.
        while(grabRightChopstick(my_rank)){
            hungry[my_rank]++;
        }
        printf("philosopher %d has both chopsticks\n", my_rank);
        usleep(eatTime);

        releaseChopsticks(my_rank);
    }

    return NULL;
}




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Attempts to grab both chopsticks at the same time.
 *
 * @param[in] rank - Identifier of philosopher/thread.
 *
 * @returns true - Both chopsticks are not available.
 * @returns false - Both chopsticks are available.
 *****************************************************************************/
bool grabChopsticks(int rank)
{
    int my_rank = (int) rank;
    int lsticknum = my_rank;
    int rsticknum;
    rsticknum = my_rank - 1;
    if (rsticknum < 0)
    {
        rsticknum = 4;
    }

    //Critical section.
    pthread_mutex_lock (&m_lock);
    if( chopstick[lsticknum] && chopstick[rsticknum])
    {
        chopstick[lsticknum] = false;//Grab left chopstick.
        chopstick[rsticknum] = false;//Grab right chopstick.
        pthread_mutex_unlock (&m_lock);
        return false;
    }
    pthread_mutex_unlock (&m_lock);

    return true;
}



/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Attempts to grab left chopstick. Locks all other
 * threads out when trying to do so.
 *
 * @param[in] rank - Identifier of philosopher/thread.
 *
 * @returns true - Left chopstick is not available.
 * @returns false - Left chopstick is available.
 *****************************************************************************/
bool grabLeftChopstick(int rank)
{
    int my_rank = (int) rank;
    int sticknum = my_rank;

    //Test and check if the chopstick is there. Grab it if it is.
    pthread_mutex_lock (&m_lock);
    if(chopstick[sticknum])
    {
        printf("philosopher %d grabbed left chopstick\n", my_rank);
        chopstick[sticknum] = false;
        pthread_mutex_unlock (&m_lock);
        return false;
    }
    pthread_mutex_unlock (&m_lock);
    return true;
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Attempts to grab right chopstick. Locks all other
 * threads out when trying to do so.
 *
 * @param[in] rank - Identifier of philosopher/thread.
 *
 * @returns true - Right chopstick is not available.
 * @returns false - Right chopstick is available.
 *****************************************************************************/
bool grabRightChopstick(int rank)
{
    int my_rank = (int) rank;
    int sticknum;

    sticknum = my_rank - 1;
    if (sticknum < 0)
    {
        sticknum = 4;
    }

    //Test and check if the chopstick is there. Grab it if it is.
    pthread_mutex_lock (&m_lock);
    if(chopstick[sticknum])
    {
        chopstick[sticknum] = false;
        printf("philosopher %d grabbed right chopstick\n", my_rank);
        pthread_mutex_unlock (&m_lock);
        return false;
    }
    pthread_mutex_unlock (&m_lock);
    return true;
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Lets go of both chopsticks
 *
 * @param[in] rank - Identifier of philosopher/thread.
 *
 * @returns none
 *****************************************************************************/
void releaseChopsticks(int rank)
{
    int my_rank = (int) rank;

    int lSticknum = my_rank;
    int rSticknum = my_rank - 1;
    if (rSticknum < 0)
    {
        rSticknum = 4;
    }

    //Double check that we actually have the chopsticks.
    pthread_mutex_lock (&m_lock);
    chopstick[lSticknum] = true;
    chopstick[rSticknum] = true;
    printf("philosopher %d released both chopsticks\n", my_rank);
    pthread_mutex_unlock (&m_lock);

}
