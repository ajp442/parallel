/** @file */
#ifndef __SORTING_UTILITIES__H__
#define __SORTING_UTILITIES__H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <sys/time.h>
#include <cmath>
#include <unistd.h>


#define LL unsigned long long int

//Functions for distributing and sorting lists.
//These typically call the below functions.
void distributeAndSort(int global_A[], int global_n, MPI_Comm comm);
//void odd_even_transposition_sort(int local_A[], int global_n, MPI_Comm comm);
int * odd_even_transposition_sort(int global_A[], int global_n, MPI_Comm comm);
void odd_even_iter(int local_A[], int temp_B[], int temp_C[], int global_size, int phase, int even_partner, int odd_partner, int my_rank, int comm_size, MPI_Comm comm);

//My sort
int * merging_sort(int global_A[], int global_size, MPI_Comm comm);
void merge(int mergebuff[], int a[], int b[], int mergeNum, int a_size);

//Supporting functions for doing the different sorts.
int compare(const void *arg1, const void *arg2);
void merge_low(int a[], int b[], int a_size, int b_size);
void merge_high(int a[], int b[], int a_size, int b_size);
void my_quick_sort(int a[], int left, int right);

//General Purpose functions that can be used almoast anywhere.
int block_low(LL rank, LL threads, LL tasks);
int block_high(int rank,int threads,int tasks);  
int block_size(int rank,int threads,int tasks);
int displacement_to_size(int displacement, int threads, int tasks);

//Debugging Functions.
void print_list(int a[], int n);
void check_list(int a[], int n);

int * funTest(int a[], int size);

#endif
