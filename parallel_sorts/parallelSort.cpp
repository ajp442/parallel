/*************************************************************************//**
 * @file
 *
 * @brief Parallel sorting. Algorythim for analysis.
 *
 * @mainpage Overview
 *
 * @section course_section Parallel Computing
 *
 * @author Andrew Pierson
 *
 * @date March 08, 2013
 *
 * @par Professor:
 *         Dr. Christer Karlsson
 *
 * @par Course:
 *         CSC 410 - M001 - 9:00am
 *
 * @section program_section Program Information
 *
 * @details 
 * 
 * The book describes a Parallel Sorting Algorithm (The Parallel Odd-Even
 * Transposition Sort). We have looked at it and analyzed it in class.  One of the
 * things we noted was that no single process ever had access or at any occasion
 * held the complete sorted list.  The algorithm made each process responsible for
 * a sub-array of size n/p. 
 * 
 * Your assignment is to implement a Parallel sorting algorithm with the following
 * specs:
 * 
 * An array of size n should be sorted.  This array is created and permutated in
 * the root process and a sub-array of size n/p is distributed to each process.
 * Each process is responsible to sort their own sub-array (use your favorite
 * sorting algorithm, but be careful, you know that the book solution is using the
 * built-in qsort(), and you will compete against this algorithm).  After the
 * sub-arrays are sorted, you will in log2(p)merge the sorted sub-arrays back to
 * the root process The final result will be that root has a fully sorted list
 * that is displayed.
 * 
 * To make this easier for you, make the following assumptions:
 * 
 * The number of processes is a power of 2 ( p = 2k ) The number of elements n is
 * divisible by p ( p|n )
 * 
 * You will then take your algorithm and compare it against the book algorithm.
 * You do need to modify the book algorithm with a step where all sorted
 * sub-arrays are gathered in the root, and then displayed.
 * 
 * Do testing on different sizes of p and for each size of p use different sizes
 * of n.
 * 
 * We are interested in how long it takes to distribute the array, sort the
 * sub-arrays and collect the sub-arrays back into a sorted array.  The timer
 * should therefore start when you begin distributing the sub-array and end as
 * soon as all sub-arrays are collected back in root.  Do not include the
 * creation, permutation and printing of the array.
 * 
 * You need to turn in the codes (both sorting versions), a spread sheet with the
 * different timing values and a short conclusion which algorithm is the faster. 
 *
 * My algorythm seems to outperform the book's but only because it uses a faster
 * quick sort.
 *
 * Instead of an odd even merge I perform a merge between two processes each step.
 * This means that there are half the nodes each time.
 *
 *
 * How to use doplot.plg script:
 * gnuplot -e "filename='problemsize100000000.dat'" doplot.plg 
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions: mpic++ -g -Wall -lm -o parallelSort parallelSort.cpp rand_lists.cpp sorting_utilities.cpp
 * There is also a make file. Use make debug for debug info. Be sure to use only sort lists as it
 * prints out the lists.
 * 
 * @par Usage:
 *    mpiexec -np <number of processes> ./parallelSort <number_of_elements>
 *    mpiexec -np <number of processes> -hostfile <list of hosts> ./parallelSort <number_of_elements>
 * 
 *
* @section todo_bugs_modification_section Todo, Bugs, and Modifications
*
* @bug [] merging sort Fails SOMETIMES seems to fail more often with larger numbers.
*
* @todo [X] Rename all the sorting and random list generation functions so they
* are more descriptive
*
* @todo [X] Add header comments to all functions.
*
* @todo [] Use valgrind to make sure I am cleaning up all the memory.
* maybe have someone else look at it if valgrind does not work. I'm not sure
* it would on a multithreaded program.
*
* @todo [Attempted - Failed] Make it so a longer list (indexed by unsigned long long ints) can
* be sorted.
*
* @todo [X] Use a thread safe random() function in my quick_generate_list()
* function.
*
* @par Modifications and Development Timeline:
* @verbatim
* Date          Modification
* ------------  --------------------------------------------------------------
* Apr 5, 2013   Programmed assigned.
* Apr 7, 2013   Read requierments.
* Apr 8, 2013   Started working on program, created files, looked at odd
*               even sort code.
* Apr 10, 2013  Looked at some parallel sorting and merging algorythms.
* Apr 11, 2013  Got program to compile and run successfully (well, just the 
*               odd-even sort portion from the book). The interface still
*               needs to be cleaned up.
* Apr 13, 2013  Finally fixed the merge high and merge low functions so the
*               sorting will work with any size list.
* Apr 15, 2013  Got the quick randomly permuted list working. Broke one large
*               file up into several smaller files. Made a Makefile for easy
*               compiling.
* Apr 16, 2013  Came up with a merege alorythim but ran into a bunch of
*               problems trying implement it.
* Apr 17, 2013  Refactored code because I was previously having every
*               process split up the large array. This caused a lot of
*               overhead.
* Apr 18, 2013  Got merge() function working. Also my merge sort is working.
*               Have debug section and implemented custom merge sort.
* Apr 19, 2013  Touched up code, did timmings and analysis.
* Apr 19, 2013  program due
*
*
* @endverbatim
*
*****************************************************************************/

#include "sorting_utilities.h"
#include "rand_lists.h"



int getUserInput(int rank);

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: This distributes threads in a communicator, comm, and has
 * them sort an array, a, of size num_elements. The beef of this function
 * distributes the array to all the processes by calling block_low() and
 * block_high() to find the offset and size for each process that it then
 * stores in two arrays, displs(displacement) and scounts(send counts). These
 * arrays are used in MPI_Scatterv() to distribute the proper section
 * of the array to each process. A sort function is then called to sort the array.
 *
 * @param[in] argc - Argument count.
 * @param[in] argv - The command-line arguments.
 *
 * @returns 0 - sucsess.
 * @returns 1 - unsuccessful execution.
 *****************************************************************************/
int main(int argc, char *argv[]) {

    int num_elements;//Total number of elements to be sorted.
    int *a;//An array to be sorted.
    int *b;//An array to be sorted.
    int rank;//The process number.
    int comm_size;
    int *result;
    MPI_Comm comm;//The communicator.

    //Timing variables.
    double start_sort1;
    double end_sort1;
    double duration_sort1;
    double start_sort2;
    double end_sort2;
    double duration_sort2;

#if DEBUG
    double start_rand;
    double end_rand;
    double duration_rand;

    int *odd_even_result;
    int *merging_result;
    int *save_a;
#endif

    //If there are command line arguments use those.
    if (argc == 2)
    {
        num_elements = strtol(argv[1], NULL, 10);
    }
    else
    {
        num_elements = getUserInput(rank);
    }


    //MPI_Init(&argc, &argv);
    MPI_Init(NULL, NULL);//Initiate.
    comm = MPI_COMM_WORLD;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);//Get the rank of the process.
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);//Get number of processes.


//-----------------------------------------------------------------------------
//                      Generate Random list.
//-----------------------------------------------------------------------------
    //Generate the list.
    if (rank == 0)
    {
        a = new int[num_elements];
        b = new int[num_elements];
#if DEBUG
        start_rand = MPI_Wtime();
        save_a = new int[num_elements];
#endif
    }
    //quick_generate_rand_list(a, num_elements, MPI_COMM_WORLD);
    //quick_generate_rand_permuted_list(a, num_elements, comm);
    //quick_generate_rand_list(a, num_elements, comm);
    if (rank == 0)
    {
        //generate_rand_permuted_list(a, num_elements);
        generate_rand_list(a, num_elements);
        memcpy(b, a, num_elements*sizeof(int));
#if DEBUG
        memcpy(save_a, a, num_elements*sizeof(int));
        end_rand = MPI_Wtime();
        duration_rand = end_rand - start_rand;
#endif
    }


//-----------------------------------------------------------------------------
//                     Odd Even Transposition Sort and Timming 
//-----------------------------------------------------------------------------
    //Start timing.
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0)
    {
        start_sort1 = MPI_Wtime();
    }

#if DEBUG
    odd_even_result = odd_even_transposition_sort(a, num_elements, comm);
#endif
    odd_even_transposition_sort(a, num_elements, comm);

    if (rank == 0)
    {
        //End timming.
        end_sort1 = MPI_Wtime();
        duration_sort1 = end_sort1-start_sort1;
    }




//-----------------------------------------------------------------------------
//                    Merging Sort and Timming 
//-----------------------------------------------------------------------------
    //Start timing.
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0)
    {
        start_sort2 = MPI_Wtime();
    }

#if DEBUG
    merging_result = merging_sort(b, num_elements, comm);
#endif
    result = merging_sort(b, num_elements, comm);
    delete [] result;

if (rank == 0)
    {
        //End timming.
        end_sort2 = MPI_Wtime();
        duration_sort2 = end_sort2-start_sort2;
    }


//-----------------------------------------------------------------------------
//                    Printing Results 
//-----------------------------------------------------------------------------
    if (rank == 0)
    {
        //End timming.
        end_sort2 = MPI_Wtime();
        duration_sort2 = end_sort2-start_sort2;
#if DEBUG
        printf("Before Sorting:\n");
        print_list(save_a, num_elements);
        printf("After Odd Even Transposition Sort:\n");
        print_list(odd_even_result, num_elements);
        printf("After Merging Sort:\n");
        print_list(merging_result, num_elements);
        printf("Random number Duration: %f\n", duration_rand);
#endif
        delete [] a;
        delete [] b;
        //printf("processes: %d\nnumer of elements: %d\nodd_even sort time: %f\nmerging  sort time: %f\n", comm_size, num_elements, duration_sort1, duration_sort2);
        printf("%d, %d, %f, %f\n", comm_size, num_elements, duration_sort1, duration_sort2);
    }

    MPI_Finalize();
    return 0;
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: If command line argumetns are not provided this function
 * is called to get user input.
 *
 *
 * @param[in] rank - Making sure only one thread gets usre input.
 *
 * @returns num_elements - The number of elements to sort.
 *****************************************************************************/
int getUserInput(int rank)
{
    if (rank == 0)
    {
        char response[256];
        int num_elements;

        //Output info about the program to the user.
        std::cout << "----------------------------------------------\n";
        std::cout << "            Parallel Sort\n";
        std::cout << "----------------------------------------------\n";
        std::cout << "Enter number of elemetns to sort: ";
        std::cin >> response;
        num_elements = (int)strtol(response, NULL, 10);
        return num_elements;
    }
    return 0;
}
