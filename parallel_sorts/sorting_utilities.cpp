/** @file */

#include "sorting_utilities.h"

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description:
 * By the end of sort all processes will have communicated with eachother and
 * now have the list sorted (porcess 0 will have the lowest numbers, process
 * 1 will have the next lowest and so on). This function relies heavily on the
 * odd_even_iter() function. The final step is to gather all the
 * sorted pieces back into the original array.
 * 
 *
 * @param[in,out] local_A - Our piece of the large array.
 * @param[in] global_n - The size of the whole array (not just our piece) that
 * is to be sorted.
 * @param[in] comm - The communicator that is to be used for sorting.
 *
 * @returns none
 *****************************************************************************/
int * odd_even_transposition_sort(int global_A[], int global_n, MPI_Comm comm)
{
    int phase;
    int local_n;
    int *temp_B;
    int *temp_C;
    int even_partner;  //phase is even or left-looking
    int odd_partner;   //phase is odd or right-looking
    int comm_size;
    int my_rank;
    int *local_A;

    int sizeOfBlock;
    int *displs;
    int *scounts;

    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);//Get number of processes.
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);//Get the rank of the process.

    sizeOfBlock = (int)block_size(my_rank, comm_size, global_n);

    // Create arrays for displacements and send counts
    local_A = new int[sizeOfBlock];
    displs = new int[comm_size]; 
    scounts = new int[comm_size];

    for (int i=0; i<comm_size; ++i) 
    {
        // Displacement is equal to the beginning of the block 
        displs[i] = (int) block_low((LL)i, (LL)comm_size, (LL)global_n);
        // Receive count is equal to the local_n for each process
        scounts[i] = (int) block_size(i, comm_size, global_n); 
    }


    MPI_Scatterv(global_A, scounts, displs, MPI_INT, local_A, sizeOfBlock , MPI_INT, 0, comm);


    local_n = block_size(my_rank, comm_size, global_n);

    //Temporary storage used in merge-split.
    temp_B = new int[(global_n/comm_size) + 1];
    temp_C = new int[(global_n/comm_size) + 1];

    //Find partners:  negative rank => do nothing during phase.
    if (my_rank % 2 != 0) 
    {
        even_partner = my_rank - 1;
        odd_partner = my_rank + 1;
        if (odd_partner == comm_size) 
        {
            odd_partner = MPI_PROC_NULL;//Idle during odd phase
        }
    }
    else 
    {
        even_partner = my_rank + 1;
        if (even_partner == comm_size) 
        {
            even_partner = MPI_PROC_NULL;//Idle during even phase
        }
        odd_partner = my_rank-1;  
    }

    //Sort local list using built-in quick sort.
    qsort(local_A, local_n, sizeof(int), compare);

    for (phase = 0; phase < comm_size; phase++)
        odd_even_iter(local_A, temp_B, temp_C, global_n, phase, even_partner, odd_partner, my_rank, comm_size, comm);


    MPI_Gatherv(local_A, sizeOfBlock, MPI_INT, global_A, scounts, displs, MPI_INT, 0, comm);


    delete [] temp_B;
    delete [] temp_C;

    delete [] displs; 
    delete [] scounts; 

    return global_A;

}

/**************************************************************************//**
 * @author Unknown -from book? Modified by Andrew Pierson
 *
 * @par Description: This does one iteration of the odd even transposition.
 *
 *
 * @param[in, out] local_A - what_it_is_for
 * @param[in] temp_B - A temporary array used for scratch work
 * @param[in] temp_C - A temporary array used for scratch work
 * @param[in] global_size - Numer of elements in the global array that we
 * are sorting.
 * @param[in] phase - The phase of the odd even iteration that we are doing.
 * @param[in] eveen_partner - The rank of our even partner, we might send
 * or receive or send to them depending on the phase.
 * @param[in] odd_partner - Rank of our odd partner, we might send or
 * receive from them depending on the phase.
 * @param[in] my_rank - Rank of current process.
 * @param[in] comm_size - The thread count, or number of processes.
 * @param[in] comm - The communicator to use.
 *
 * @returns none.
 *****************************************************************************/
void odd_even_iter(int local_A[], int temp_B[], int temp_C[], int global_size, int phase, int even_partner, int odd_partner, int my_rank, int comm_size, MPI_Comm comm)
{
    int local_n;
    int even_partner_n;
    int odd_partner_n;
    MPI_Status status;

    local_n = block_size(my_rank, comm_size, global_size); 
    even_partner_n = block_size(even_partner, comm_size, global_size);
    odd_partner_n = block_size(odd_partner, comm_size, global_size);

    //Even phase.
    if (phase % 2 == 0) 
    {
        if (even_partner >= 0) 
        {
            //int MPI_Sendrecv(void *sendbuf, int sendcount, MPI_Datatype sendtype, int dest, int sendtag, void *recvbuf, int recvcount, MPI_Datatype recvtype, int source, int recvtag, MPI_Comm comm, MPI_Status *status)
            MPI_Sendrecv(local_A, (int)local_n, MPI_INT, even_partner, 0, temp_B, (int)even_partner_n, MPI_INT, even_partner, 0, comm, &status);
            if (my_rank % 2 != 0)
            {
                merge_high(local_A, temp_B, local_n, even_partner_n);
            }
            else
            {
                merge_low(local_A, temp_B, local_n, even_partner_n);
            }
        }
    }

    //Odd phase.
    else 
    { 
        if (odd_partner >= 0) 
        {
            MPI_Sendrecv(local_A, (int)local_n, MPI_INT, odd_partner, 0, temp_B, (int)odd_partner_n, MPI_INT, odd_partner, 0, comm, &status);
            if (my_rank % 2 != 0)
            {
                merge_low(local_A, temp_B, local_n, odd_partner_n);
            }
            else
            {
                merge_high(local_A, temp_B, local_n, odd_partner_n);
            }
        }
    }
}







/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Merges two sorted arrays into one large sorted array.
 * The size of the b array is calculated by the difference between the 
 * a_size and mergeNum.
 *
 * 
 *
 * @param[in, out] mergbuff - The larger array in which to merege the two
 * smaller arrays.
 * @param[in] a - A sorted array to merege.
 * @param[in] b - A sorted array to merege.
 * @param[in] mergeNum - Number of elements to merge into the array that is
 * going to be returned.
 * @param[in] a_size - The size of the a array.
 *
 * @returns item - why_+_description
 *****************************************************************************/
void merge(int mergebuff[], int a[], int b[], int mergeNum, int a_size)
{
    int a_i = 0;//a array iterator.
    int b_i = 0;//b array iterator.
    int m_i = 0;//merge array iterator.
    int b_size = mergeNum - a_size;//The size of the b array.
    bool flag = false;//Flag indicating if b_i is > b_size.


    //For every element in the large array,
    //merge the two smaller arrays into it.
    for (m_i=0; m_i<mergeNum; m_i++)
    {
        if ((a_i < a_size && a[a_i] < b[b_i] ) || flag)
        {
            mergebuff[m_i] = a[a_i];
            a_i++;
        }
        else if (b_i < b_size)
        {
            mergebuff[m_i] = b[b_i];
            b_i++;
        }
        else
        {
            flag = true;
            m_i--;
        }
    }
}




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Disperses array global_A[], to all the processes in comm.
 * Each process sorts their array with a costum quick sort. Each of the sorted
 * arrays are merged down in log2(p) steps where p is the number of processes.
 * I would also like to note that this sort can take any number of processes
 * and any number of elements (as long as there are more elements than
 * processes.
 *
 * @param[in] global_A - The array to sort.
 * @param[in] global_n - The number of elements in the array.
 * @param[in] comm - The communicator that will be used to sort the array.
 *
 * @returns ret - A pointer to the sorted array.
 *****************************************************************************/
int * merging_sort(int global_A[], int global_n, MPI_Comm comm)
{
    int steps;
    int step;

    int twoToTheStep;
    int twoToTheStepMinusOne;

    int *recvbuff;
    int *mergebuff;
    int *ret;
    int recvNum;
    int mergeNum;
    int sender;
    int receiver;
    int local_n;
    int comm_size;
    int my_rank;

    int sizeOfBlock;
    int *displs;
    int *scounts;

    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);//Get number of processes.
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);//Get the rank of the process.
    sizeOfBlock = block_size(my_rank, comm_size, global_n);

    //local_A = new int[sizeOfBlock];
    ret = new int[sizeOfBlock];
    displs = new int[comm_size]; 
    scounts = new int[comm_size];

    for (int i=0; i<comm_size; ++i) 
    {
        // Displacement is equal to the beginning of the block 
        displs[i] = block_low((LL)i, (LL)comm_size, (LL)global_n);
        // Receive count is equal to the local_n for each process
        scounts[i] = block_size(i, comm_size, global_n); 
    }

    MPI_Scatterv(global_A, scounts, displs, MPI_INT, ret, sizeOfBlock , MPI_INT, 0, comm);


    local_n = sizeOfBlock;
    steps = ceil(log2(comm_size));

    //Tis is where you can implement any sort you want. I choose to do a custom quick sort
    //instead of the STL qsort().
    //qsort(ret, (int)local_n, sizeof(int), compare);//I used the qsort() to compare my
                                                     //merging algorithm to the 
                                                     //books odd even transposition.
    my_quick_sort(ret, 0, local_n-1);

    for (step = 1; step<=steps; step++)
    {
        twoToTheStep = (int)pow(2, step);
        twoToTheStepMinusOne = (int)pow(2, step-1);
        if(my_rank%twoToTheStep == 0){
            if(my_rank+twoToTheStepMinusOne<comm_size){
                sender = my_rank+twoToTheStepMinusOne;
                MPI_Recv(&recvNum, 1, MPI_INT, sender, 0, comm, MPI_STATUS_IGNORE);
                recvbuff = new int [recvNum];
                MPI_Recv(recvbuff, recvNum, MPI_INT, sender, 0, comm, MPI_STATUS_IGNORE);
                mergeNum = recvNum + local_n;
                mergebuff = new int[mergeNum];
                merge(mergebuff, ret, recvbuff, mergeNum, local_n);
                delete [] recvbuff;

                //Transfer what's in the mergebuff to our local_A array.
                local_n = mergeNum;
                delete [] ret;
                ret = new int[local_n];
                memcpy(ret, mergebuff, mergeNum*sizeof(int));
                delete [] mergebuff;
            }
        }
        else if(my_rank%twoToTheStepMinusOne == 0){
            if(my_rank-twoToTheStepMinusOne >= 0){
                receiver = my_rank-twoToTheStepMinusOne;
                MPI_Send(&local_n, 1, MPI_INT, receiver, 0, comm);
                MPI_Send(ret, local_n, MPI_INT, receiver, 0, comm);
            }
        }
    }

    delete [] displs; 
    delete [] scounts; 

    return ret;

}






















/**************************************************************************//**
 * @author Dr. Logar
 *
 * @par Description: This is a code snipped from one of Dr. Logar's examples.
 * It is a compare function for the system quicksort.
 *
 * 
 *
 * @param[in] arg1 - First value to be compared.
 * @param[in] arg2 - Second value to be compared.
 *
 * @returns 1 - if arg1 > arg2
 * @returns -1 - if arg1 < arg2
 * @returns 0 - if arg1 = arg2
 *****************************************************************************/
int compare(const void *arg1, const void *arg2)
{
    int *ip1;
    int *ip2;
    int  i1;
    int  i2;

    ip1 = (int *) arg1;
    ip2 = (int *) arg2;
    i1 = *ip1;
    i2 = *ip2;
    if (i1 < i2)
        return -1;
    if (i1 > i2)
        return +1;
    return 0;
} 

/**************************************************************************//**
 * @author Unknown
 *
 * @par Description: Merges the smallest merge_num elements from my_array and
 * recv into temp. It then coppies temp back into my_array.
 *
 *
 * @param[in,out] a - The first array to merge.
 * @param[in] b - The second array to merege.
 * @param[in] a_size - The size of the a[] array.
 * @param[in] b_size - The size of the b[] array.
 *
 * @returns none.
 *****************************************************************************/
void merge_low(int a[], int b[], int a_size, int b_size) 
{
    int a_i = 0;//a iterator.
    int b_i = 0;//b iterator.
    int t_i = 0;//temp iterator.
    int *temp;


    //Create temporary array.
    temp = new int[a_size];


    //Start at the begging of temp and start filling it with only the
    //lowest values.
    while (t_i < a_size) {
        if (b_i <= b_size-1 && a[a_i] > b[b_i] )
        {
            temp[t_i] = b[b_i];
            t_i++; b_i++;
        }
        else
        {
            temp[t_i] = a[a_i];
            t_i++; a_i++;
        }
    }


    //Transfer all the lowest values that we had in temp into
    //a[], which we will return.
    memcpy(a, temp, a_size*sizeof(int));


    delete [] temp;
}  

/**************************************************************************//**
 * @author Unknown
 *
 * @par Description: Merges the largest elements from a[] and
 * b[] into temp. It then coppies temp back into a[]. Since we start looking
 * at the back of the array we must make sure we the the last element in both
 * arrays, therefore we need b's size.
 *
 *
 * @param[in,out] a - The array to merge.
 * @param[in] b - The second array to merege.
 * @param[in] a_size - The size of the a array.
 * @param[in] b_size - The size of the b array.
 *
 * @returns none.
 *****************************************************************************/
void merge_high(int a[], int b[], int a_size, int b_size) 
{

    int a_i = a_size-1;//a iterator.
    int b_i = b_size-1;//b iterator.
    int t_i = a_size-1;//temp iterator.
    int *temp;//A temporary array.


    //Create temporary array.
    //The temporary array must be the same size as the array we are returning.
    temp = new int[a_size];

    //We have to fill temp[] with only the highest values.
    //So start at the last position in temp and work our way
    //forward filling it with only the highest value.
    while (t_i >= 0) {
        if (b_i >= 0 && a[a_i] < b[b_i]) 
        {
            temp[t_i] = b[b_i];
            t_i--; b_i--;
        }
        else
        {
            temp[t_i] = a[a_i];
            t_i--; a_i--;
        }
    }


    //Do a coppy that properly coppies the correct values into a.
    //It is a safe version of memcopy that perserves the highest numbers
    //in the case that a_size is larger than size.
    memcpy(a, temp, a_size*sizeof(int));

    //Clear up memory.
    delete [] temp;
} 



/**************************************************************************//**
 * @author Andrew Pierson
 * @par Description: Used for calculating the load for each thread.
 * @param[in] rank - ID of a thread so to speak.
 * @param[in] threads - The total number of threads that will be ran.
 * @param[in] tasks - The total number of tasks that needs to be divided.
 * @returns size - The number of taks for a particular process.
 *****************************************************************************/
int block_size(int rank,int threads,int tasks){
    return (block_high((int) rank, (int) threads,(int)tasks)-block_low((LL)rank,(LL)threads,(LL)tasks)+1);
}
/**************************************************************************//**
 * @author Andrew Pierson
 * @par Description: Used for finding the starting index of a chunck of data
 * for a particular thread. Here the rank, threads and tasks are 
 * unsigned long long ints. This reduces overflow errors when multiplying
 * tasks and rank.
 * @param[in] rank - ID of a thread so to speak.
 * @param[in] threads - The total number of threads that will be ran.
 * @param[in] tasks - The total number of tasks that needs to be divided.
 * @returns start - The starting index for a particular thread.
 *****************************************************************************/
int block_low(LL rank, LL threads, LL tasks){
    return (int)((rank)*(tasks)/(threads));
}
/**************************************************************************//**
 * @author Andrew Pierson
 * @par Description: Used for finding the ending index of a chunck of data
 * for a particular thread.
 * @param[in] rank - ID of a thread so to speak.
 * @param[in] threads - The total number of threads that will be ran.
 * @param[in] tasks - The total number of tasks that needs to be divided.
 * @returns end - The ending index for a particular thread.
 *****************************************************************************/
int block_high(int rank, int threads, int tasks){  
    return (int)(block_low((LL)(rank+1),(LL)threads,(LL)tasks)-1);
}
/**************************************************************************//**
 * @author Andrew Pierson
 * @par Description: Used for calculating the load at a certain displacement.
 * @param[in] rank - ID of a thread so to speak.
 * @param[in] threads - The total number of threads that will be ran.
 * @param[in] tasks - The total number of tasks that needs to be divided.
 * @returns size - The number of tasks.
 *****************************************************************************/
int displacement_to_size(int displacement, int threads, int tasks)
{
    int rank;
    rank = (displacement*threads)/tasks;
    return block_size(rank, threads, tasks);
}

















































/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Prints out a list. This is exclusively for debugging
 * purposes as arrays are usually too large.
 *
 *
 * @param[in] a - The array to print.
 * @param[in] n - The number of elements in the array.
 *
 * @returns none.
 *****************************************************************************/
void print_list(int a[], int n)
{
    int i;

    for (i = 0; i < n; i++)
        std::cout << "  " << a[i];
    std::cout << std::endl;
}



/**************************************************************************//**
 * @author Dr. Logar
 *
 * @par Description: This is a code snipped from one of Dr. Logar's examples.
 * Checks if a list is on order. Good for debugging.
 *
 * 
 *
 * @param[in] a - The array to check.
 * @param[in] n - The number of elements in the array.
 *
 * @returns none.
 *****************************************************************************/
void check_list(int a[], int n)
{
    int err_count = 0;
    int i;

    for (i = 0; i < n - 1; i++)
        if (a[i] > a[i + 1])
        {
            std::cout << "***** ERROR FOUND IN ORDER *****" << std::endl;
            err_count++;
            if (err_count > 10)
            {
                std::cout << "***** TOO MANY ERRORS *****" << std::endl;
                exit(1);
            }
        }
    std::cout << " Largest number is " << a[n - 1] << std::endl;
    if (err_count == 0)
        std::cout << " List is in order" << std::endl;
}



/**************************************************************************//**
 * @author Dr. Logar
 *
 * @par Description: This uses the insertion sort when the sublist size
 * is 15 or less.
 *
 *
 * @param[in, out] a - The array to be sorted.
 * @param[in] left - The left most position to sort.
 * @param[in] right - The rigt position to sort.
 *
 * @returns item - why_+_description
 *****************************************************************************/
void my_quick_sort(int a[], int left, int right)
{
    int      i;
    int      j;
    int      pivot;
    int      temp;

    if (right - left < 1)
        return;
    if (right - left < 15)
    {
        for (i = left + 1; i <= right; i++)
        {
            temp = a[i];
            j = i;
            while ((j > left) && (temp < a[j-1]))
            {
                a[j] = a[j - 1];
                j--;
            } // while
            a[j] = temp;
        }  // for i
        return;
    }

    if (left < right)  // check is redundant, left here to mirror other versions
    {
        i = left;
        j = right + 1;
        pivot = a[left];
        do
        { 
            do
            {
                i++;
            }  while (a[i] < pivot && i<=right);
            // stops if num is >= pivot
            do
            {
                j--;
            }  while (a[j] > pivot && j>=left);
            // stops if num <= pivot

            if (i < j)
            {
                temp = a[i];
                a[i] = a[j];
                a[j] = temp; 
            }
        }  while (i < j);

        // swap pivot a[left] with "right" a[j]
        temp = a[j];
        a[j] = a[left];
        a[left] = temp; 

        my_quick_sort(a, left, j - 1);
        my_quick_sort(a, j + 1, right);

    }
} 

