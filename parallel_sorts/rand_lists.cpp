/** @file */

#include "rand_lists.h"

void generate_rand_permuted_list(int a[], int num_elements)
{
        for (int i = 0; i < num_elements; i++)
        {
            a[i] = i;
        }

        rand_permute(a, 0, num_elements-1);
}


/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Generates a randomly permuted sequence of numbers from 0
 * to num_elements. Each process fills each portion of the large array with
 * numbers that are the same as the large array's index (i.e. index 231 will
 * hold the value 231). Each process then mixes up all the numbers in its
 * range. These random sections of the large array are put back together in
 * the large array in random segments. The overall randomness is quite poor
 * but good enough for testing.
 *
 * @bug [] Does not work in some cases. Fails on MPI_Scatterv(). From what
 * I have tested, all values below 260000000 work and no values above 270000000
 * work when running 8 threads. My guess is that block_size() and/or block_high()
 * are returning wrong values becasue they are overflowing integers when they do
 * multiplication.
 * 
 *
 * @param[in,out] a - An array to fill with randomly permuted sequence.
 * @param[in] num_elements - Number of elements in the array.
 * @param[in] comm - The communicator of processes that will generate said array.
 *
 * @returns none.
 *****************************************************************************/
void quick_generate_rand_permuted_list(int a[], int num_elements, MPI_Comm comm)
{  
    int my_rank;
    int comm_sz;
    int *local_A;
    int i;
    int sizeOfBlock;
    int start;
    int end;
    int *displs = NULL;
    int *scounts = NULL;

    MPI_Comm_size(comm, &comm_sz);
    MPI_Comm_rank(comm, &my_rank); 

    sizeOfBlock = block_size(my_rank, comm_sz, num_elements);
    start = block_low((LL) my_rank, (LL) comm_sz, (LL)num_elements);
    end = block_high((int) my_rank, (int) comm_sz, num_elements);

    // Create arrays for displacements and send counts
    displs = new int[comm_sz]; 
    scounts = new int[comm_sz];
    local_A = new int[sizeOfBlock];

    for (i=0; i<comm_sz; ++i) 
    {
        // Displacement is equal to the beginning of the block 
        displs[i] = block_low((LL) i, (LL) comm_sz, (LL)num_elements);
        // Receive count is equal to the local_n for each process
        scounts[i] = block_size(i, comm_sz, num_elements); 
    }  

    //int MPI_Scatterv( void *sendbuf, int *sendcnts, int *displs, MPI_Datatype sendtype, void *recvbuf, int recvcnt, MPI_Datatype recvtype, int root, MPI_Comm comm)
    MPI_Scatterv(a, (int *)scounts, (int *)displs, MPI_INT, local_A, (int)sizeOfBlock, MPI_INT, 0, comm);

    create_rand_section(local_A, start, end);

    if (my_rank == 0)
    {
        mix_it_up(displs, scounts, 0, comm_sz-1);
    }
    MPI_Barrier(comm);
    //MPI_Bcast(void* data, int count, MPI_Datatype datatype, int root, MPI_Comm communicator)
    MPI_Bcast(displs, comm_sz, MPI_INT, 0, comm);
    MPI_Bcast(scounts, comm_sz, MPI_INT, 0, comm);

    //int MPI_Gatherv(void *sendbuf, int sendcnt, MPI_Datatype sendtype, void *recvbuf, int *recvcnts, int *displs, MPI_Datatype recvtype, int root, MPI_Comm comm)
    MPI_Gatherv(local_A, (int)scounts[my_rank], MPI_INT, a, (int *)scounts, (int *)displs, MPI_INT, 0, comm);

    delete [] displs;
    delete [] scounts;
    delete [] local_A;

}

void create_rand_section(int a[], int start, int end)
{
    int i;
    int j = 0;

    for (i = start; i <= end; i++)
    {
        a[j] = i;
        j++;
    }

    rand_permute(a, start, end);
}

void rand_permute(int a[], int start, int end)
{
    int i, j, k, l;
    struct drand48_data drand_buff;
    double r;

    //Seed the random number generator.
    srand48_r(time(NULL), &drand_buff);

    l = 0;

    for(i=start; i<(end); i++)
    {
        drand48_r(&drand_buff, &r);
        j = (i+(int)((end+1-i)*(r/1)))-start;//number between current end of array.
        k = a[l];//Save what is in our current position.
        a[l] = a[j];//Move whatever is in that random place to our current position.
        a[j] = k;//Move what WAS in our current position to the random position.
        l++;
    }

}

void generate_rand_list(int a[], int num_elements)
{
    int i;

    srand(time(NULL));

    for (i = 0; i < num_elements; i++)
    {
        a[i] = rand();
    }
}



void quick_generate_rand_list(int a[], int num_elements, MPI_Comm comm)
{
    int my_rank;
    int comm_sz;
    int *local_A;
    int i; 
    int j;
    int sizeOfBlock;
    int *displs = NULL;
    int *scounts = NULL;
    struct drand48_data drand_buff;
    double r;

    MPI_Comm_size(comm, &comm_sz);
    MPI_Comm_rank(comm, &my_rank); 

    sizeOfBlock = block_size(my_rank, comm_sz, num_elements);

    // Create arrays for displacements and send counts
    displs = new int[comm_sz]; 
    scounts = new int[comm_sz];
    local_A = new int[sizeOfBlock];

    for (j=0; j<comm_sz; ++j) 
    {
        // Displacement is equal to the beginning of the block 
        displs[j] = block_low((LL)j, (LL)comm_sz, (LL)num_elements);
        // Receive count is equal to the local_n for each process
        scounts[j] = block_size(j, comm_sz, num_elements); 
    }  

    MPI_Scatterv(a, (int *)scounts, (int *)displs, MPI_INT, local_A, (int)sizeOfBlock, MPI_INT, 0, comm);


    //Seed the random number generator.
    srand48_r(time(NULL)+my_rank, &drand_buff);

    for(i=0; i<sizeOfBlock; i++)
    {
        drand48_r(&drand_buff, &r);
        local_A[i] = r*INT_MAX;
    }

    MPI_Gatherv(local_A, (int)scounts[my_rank], MPI_INT, a, (int *)scounts, (int *)displs, MPI_INT, 0, comm);

    delete [] displs;
    delete [] scounts;
    delete [] local_A;
}

void mix_it_up(int displs[],int scounts[], int start, int end)
{
    int i, j, k, l, p;
    struct drand48_data drand_buff;
    double r;

    l = 0;

    srand48_r(time(NULL), &drand_buff);


    for(i=start; i<(end); i++)
    {
        drand48_r(&drand_buff, &r);
        j = (i+(int)((end+1-i)*(r/2)))-start;//number between current end of array.
        if(scounts[l] == scounts[j])
        {
            k = displs[l];//Save what is in our current position.
            p = scounts[l];

            displs[l] = displs[j];//Move whatever is in that random place to our current position.
            scounts[l] = scounts[j];
            displs[j] = k;//Move what WAS in our current position to the random position.
            scounts[j] = p;
        }
        l++;
    }
}

