/** @file */

#ifndef __RAND_LISTS__H__
#define __RAND_LISTS__H__


#include "sorting_utilities.h"
#include <limits.h>

//Functions for distributing workload and creating random lists.
void generate_rand_permuted_list(int a[], int num_elements);
void quick_generate_rand_permuted_list(int a[], int num_elements, MPI_Comm comm);
void generate_rand_list(int a[], int num_elements);
void quick_generate_rand_list(int a[], int num_elements, MPI_Comm comm);


//Helper functions.
void create_rand_section(int a[], int start, int end);
void rand_permute(int a[], int start, int end);
void mix_it_up(int displs[],int scounts[], int start, int end);



#endif
