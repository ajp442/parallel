 #! /bin/bash

 #The output may need to be tweeked
 #so that only the time is displayd
 #for this script to run well and look nice.
 
 #Clear the outputfile
 ''>output.txt
 file='../../open.hosts'
 #for each number of processors
 for j in 10000000 50000000 100000000
 do
     #and for every problem size
     #echo "number of processors: $i" | tee -a output.txt
     for i in {1..64}
     do
         #run the program with the number of processors using the
         #variable j as input to the program
         #tee will output the values to screen and to an output file
         #echo -n "$j, " | tee -a output.txt
         mpiexec -np $i -hostfile $file ./parallelSort $j | tee -a output.txt
     done
 done
