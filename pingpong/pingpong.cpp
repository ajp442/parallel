/*************************************************************************//**
 * @file
 *
 * @brief Calculates the communication cost (bandwith and latency) on your
 * parallel computer.
 *
 * @mainpage Overview
 *
 * @section course_section Parallel Computing
 *
 * @author Andrew Pierson
 *
 * @date April 03, 2013
 *
 * @par Professor:
 *         Dr. Christer Karlsson
 *
 * @par Course:
 *         CSC 410 - 9:00am
 *
 *
 * @section program_section Program Information
 *
 * @details Detailed_description_of_the_program
 * A ping-pong is a communication in which two messages are sent, first from
 * process A to process B (ping) and then from B back to process A (pong).
 * Timing blocks of repeated ping-pongs is common way to estimate the
 * communication cost.  Assume that the time needed to send an n-byte message is
 * λ+n/β. Write a program implementing the “ping pong” test to determine λ
 * (latency) and β (bandwidth) on your parallel computer. Design the program to
 * run on exactly two processes (on two different nodes, or machines).  Process 0
 * records the time and then sends a message to process 1.  After process 1
 * receives the message, it immediately sends it back to process 0.  Process 0
 * receives the message and records the time. The elapsed time divided by 2 is
 * the average message-passing time.  Try sending messages multiple times, and
 * experiment with message of different lengths, to generate enough data points
 * that you can estimate λ and β.
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *    mpic++ -g -Wall -lm -o pingpong pingpong.cpp
 *
 * @par Usage:
 *    mpiexec -np <number of processes> ./pingpong
 *    mpiexec -np <number of processes> -hostfile <list of hosts> ./pingpong <msg_size> <send_iterations>
 *
 * @verbatim
 *
 * DESCRIPTION
 *
 *
 * @endverbatim
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 * @bug [] program hangs and does not end.
 *
 *
 * @todo
 *
 * @par Modifications and Development Timeline:
 * @verbatim
 * Date          Modification
 * ------------  --------------------------------------------------------------
 * Mar 01, 2013   Program assigned
 * Apr 02, 2013   Started working program.
 * Apr 03, 2013   Program due
 * @endverbatim
 *
 *****************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <sys/time.h>

#if DEBUG
#include <map>
#include <iostream>
#include <iomanip>
#endif

int main(int argc, char *argv[]) {
    int thread_count;//Number of processes.
    int message_size;
    int send_iterations;
    int rank;//Rank (ID) of each thread.
    char *message;
    char *messagebuff;
    char *messageoriginal;

    //Timing variables.
    double start;
    double end;
    double duration;
    double average_duration;


#if DEBUG
    //Debug variables.
    std::map<std::string,int> mymap;
    std::map<std::string,int>::iterator it;
    std::string processor;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int namelen;
#endif


    //MPI_Init(&argc, &argv);
    MPI_Init(NULL, NULL);//Initiate.
    MPI_Comm_size(MPI_COMM_WORLD, &thread_count);//Get number of processes.
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);//Get the rank of the process.

#if DEBUG
    MPI_Get_processor_name(processor_name, &namelen);
#endif




    //If there are command line arguments use those.
    if (argc == 3)
    {
        message_size = strtol(argv[1], NULL, 10);
        send_iterations = strtol(argv[2], NULL, 10);
    }
    else
    {
        std::cout << "Invalid arguments\n";
        //message_size = getUserInput(rank);
    }

    //Create the message.
    message = new (std::nothrow) char[message_size];
    messagebuff = new (std::nothrow) char[message_size];
    messageoriginal = new (std::nothrow) char[message_size];
    memset(messageoriginal, 'a', message_size);
    messageoriginal[message_size - 1] = (char)NULL;
    strcpy(message, messageoriginal);

    //Start timing.
    MPI_Barrier(MPI_COMM_WORLD);
    start = MPI_Wtime();

    for (int i = 0; i<send_iterations; i++)
    {

        if (rank != 0)
        {
            MPI_Recv(message, message_size, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            for (int i = 0; i < message_size-1; i++)
            {
                message[i] = message[i] + 1;
            }
            MPI_Send(message, message_size, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
        }

        if (rank == 0) 
        {
            MPI_Send(message, message_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
            MPI_Recv(messagebuff, message_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            strcpy(message, messagebuff);
        }
    }


#if DEBUG
    if (rank != 0)
    {
            MPI_Send(processor_name, strlen(processor_name) + 1, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    }
    if (rank == 0)
    {
        std::cout << "-----------------------------DEBUG INFO----------------------------------\n";
        std::cout << "Main thread running on: " << processor_name << std::endl;
        processor = processor_name;
        mymap.insert ( std::pair<std::string,int>(processor,1) );

        for (int q = 1; q < thread_count; q++){
            MPI_Recv(processor_name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            processor = processor_name;
            it = mymap.find(processor);
            if (it == mymap.end())
            {
                //Insert it with a count of 1.
                mymap.insert ( std::pair<std::string,int>(processor,1) );
            }
            else 
            {
                //Increment it's value.
                it->second ++;
            }
        }

        //What processors are being used and how many threads on each.:
        std::cout << "Processor Name" << "\t\t\t" << "Number of threads\n";
        for (std::map<std::string,int>::iterator it=mymap.begin(); it!=mymap.end(); ++it)
            std::cout << it->first << "\t\t"  << it->second << '\n';
        std::cout << "-------------------------------------------------------------------------\n";
    }
#endif

    if (rank == 0)
    {
        //Calculate time it took.
        end = MPI_Wtime();
        duration = end - start;
        average_duration = duration/send_iterations;
        //Output our findings.
        printf("input: %s\n", messageoriginal);
        printf("output: %s\n", messagebuff);
        printf("Average duration: %lf\n" , average_duration/2);
    }
    MPI_Finalize();
    return 0;
}
