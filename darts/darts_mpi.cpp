/*************************************************************************//**
 * @file
 *
 * @brief Monte Carlo estimation of pi via simulation similar to throwing
 * darts at a dart board.
 *
 * @mainpage Overview
 *
 * @section course_section Parallel Computing
 *
 * @author Andrew Pierson
 *
 * @date March 08, 2013
 *
 * @par Professor:
 *         Dr. Christer Karlsson
 *
 * @par Course:
 *         CSC 410 - M001 - 9:00am
 *
 * @section program_section Program Information
 *
 * @details 
 * Suppose we toss darts randomly at a square dartboard, where the bullseye
 * is at the origin, and whose sides are 2 ft in length.  Suppose also that
 * there is a circle with radius 1 ft inscribed in the square dartboard. If the
 * points that are hit by the darts are uniformly distributed (and always
 * landing in the square), then the number of darts that hit inside the circle
 * should approximately satisfy the equation:
 *
 * number_in_circle/total_number_of_darts = pi/4
 *
 * Since the ratio of the area of the circle to the area of the square is π/4.
 * We can use this formula to estimate the value of π with a random number
 * generator.
 *
 * This is a so called Monte Carlo process, as it uses randomness to solve a 
 * problem.
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *    mpic++ -g -Wall -lm -o darts_mpi darts_mpi.cpp
 *
 * @par Usage:
 *    mpiexec -np <number of processes> ./darts_mpi
 *    mpiexec -np <number of processes> -hostfile <list of hosts> ./darts_mpi <num_darts>
 * 
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 *
 * @todo
 *
 * @par Modifications and Development Timeline:
 * @verbatim
 * Date          Modification
 * ------------  --------------------------------------------------------------
 * Mar 1, 2013   Program assigned
 * Mar 9, 2013   Cranked out the whole program.(Like a Boss)
 * @endverbatim
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <sys/time.h>

#if DEBUG
#include <map>
#include <iostream>
#include <iomanip>
#endif

//-------#defines------------
#define LONG unsigned long long int

//----Function prototypes--------
LONG getUserInput(int rank);
LONG throwDarts(LONG myTosses);
LONG block_low(LONG rank,LONG threads,LONG tasks);
LONG block_high(LONG rank,LONG threads,LONG tasks);  
LONG block_size(LONG rank,LONG threads,LONG tasks);
struct timeval timeval_subtract(struct timeval start, struct timeval end);


int main(int argc, char *argv[]) {
    int thread_count;//Number of processes.
    int rank;//Rank (ID) of each thread.
    LONG total_in_circle = 0;//Total # of darts that land in the circle.
    LONG total_tosses = 1000000000;//The grand total number of tosses that will be used to calculate pi.
    LONG number_in_circle;//Local number of darts that land in the circle
    LONG myTosses;//Number of tosses a particular process gets.
    long double pi_estimate;//The final estimate for pi.

    //Timing variables.
    struct timeval start;
    struct timeval end;
    struct timeval duration;


#if DEBUG
    //Debug variables.
    std::map<std::string,int> mymap;
    std::map<std::string,int>::iterator it;
    std::string processor;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int namelen;
#endif


    //MPI_Init(&argc, &argv);
    MPI_Init(NULL, NULL);//Initiate.
    MPI_Comm_size(MPI_COMM_WORLD, &thread_count);//Get number of processes.
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);//Get the rank of the process.
#if DEBUG
    MPI_Get_processor_name(processor_name, &namelen);
#endif




    //If there are command line arguments use those.
    if (argc == 2)
    {
        total_tosses = strtol(argv[1], NULL, 10);
    }
    else
    {
        total_tosses = getUserInput(rank);
    }



    
    //Start timing.
    MPI_Barrier(MPI_COMM_WORLD);
    gettimeofday(&start, 0);


    //Calculate the number of tosses each thread does.
    myTosses = block_size(rank, thread_count, total_tosses);
    //Now have each thread throw thier alotted number of darts.
    number_in_circle = throwDarts(myTosses);

    //Round up all the tosses from every thread.
    MPI_Reduce(&number_in_circle, &total_in_circle, 1, MPI_UNSIGNED_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

#if DEBUG
    if (rank != 0)
    {
        MPI_Send(processor_name, strlen(processor_name) + 1, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    }
#endif

    //Have thread 0 do the final calculations and output information.
    if (rank == 0) 
    {
        //End timming.
        //Why do it here? not quite sure, it's what the book had.
        gettimeofday(&end, 0);
        //Calculate time it took.
        duration = timeval_subtract(start, end);
        //Output our findings.
        pi_estimate = ((4.0*total_in_circle)/total_tosses);
        printf("pi estimate: %Lf\n", pi_estimate);
        printf("Time: %ld.%06ld\n" ,duration.tv_sec, duration.tv_usec);

#if DEBUG
        std::cout << "-----------------------------DEBUG INFO----------------------------------\n";
        std::cout << "Main thread running on: " << processor_name << std::endl;
        processor = processor_name;
        mymap.insert ( std::pair<std::string,int>(processor,1) );

        for (int q = 1; q < thread_count; q++){
            MPI_Recv(processor_name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            processor = processor_name;
            it = mymap.find(processor);
            if (it == mymap.end())
            {
                //Insert it with a count of 1.
                mymap.insert ( std::pair<std::string,int>(processor,1) );
            }
            else 
            {
                //Increment it's value.
                it->second ++;
            }
        }

        //What processors are being used and how many threads on each.:
        std::cout << "Processor Name" << "\t\t\t" << "Number of threads\n";
        for (std::map<std::string,int>::iterator it=mymap.begin(); it!=mymap.end(); ++it)
            std::cout << it->first << "\t\t"  << it->second << '\n';
        std::cout << "-------------------------------------------------------------------------\n";
#endif
    }


    MPI_Finalize();
    return 0;
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Simulates throwing darts at a dart board. Returns the
 * number of darts that were on target.
 *
 * @param[in] tosses - The number of tosses this thread will do to calculate
 * its portion of pi.
 *
 * @returns dartsOnTarget - The number of darts that hit the target.
 *****************************************************************************/
LONG throwDarts(LONG myTosses)
{
    double x = 0;//X location of dart toss.
    double y = 0;//Y location of dart toss.
    double distFromCenter;//Distance the dart is from the center.
    LONG dartsOnTarget = 0;//Darts that ended up in circle.
    LONG toss;//Iterator to keep track of tosses.
    struct drand48_data drand_buff;

    //Seed the random number generator.
    srand48_r(time(NULL), &drand_buff);


    //Toss all darts and keep track of the ones that land inside the circle.
    for (toss = 0; toss < myTosses; toss++)
    {
        //Generate and x an y between 0 and 1.
        drand48_r(&drand_buff, &x);
        drand48_r(&drand_buff, &y);
        
        distFromCenter = (x*x + y*y);
        if (distFromCenter <= 1)
        {
            dartsOnTarget ++;
        }
    }

    return dartsOnTarget;
}


LONG getUserInput(int rank)
{
    if (rank == 0)
    {
        char response[256];
        LONG total_tosses;

        //Output info about the program to the user.
        std::cout << "----------------------------------------------\n";
        std::cout << "            DARTS, estimation of pi\n";
        std::cout << "----------------------------------------------\n";
        std::cout << "Monte Carlo estimtation of pi by throwing darts at a dart board.\n";
        std::cout << "Enter number of darts to throw: ";
        std::cin >> response;
        total_tosses = (LONG)strtol(response, NULL, 10);
        return total_tosses;
    }
    return 0;
}




/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Used for calculating the load for each thread.
 *
 *
 * @param[in] rank - ID of a thread so to speak.
 * @param[in] threads - The total number of threads that will be ran.
 * @param[in] tasks - The total number of tasks that needs to be divided.
 *
 * @returns size - The number of taks for a particular process.
 *****************************************************************************/
LONG block_size(LONG rank,LONG threads,LONG tasks){
    return (block_high(rank,threads,tasks)-block_low(rank,threads,tasks)+1);
}
LONG block_low(LONG rank,LONG threads,LONG tasks){
    return ((rank)*(tasks)/(threads));
}
LONG block_high(LONG rank,LONG threads,LONG tasks){  
   return (block_low((rank)+1,threads,tasks)-1);
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Subtracts two timeval structures, results in the 
 * time difference between the two. timeval is broken
 * up into two parts; seconds and micro seconds.
 *
 *
 * @param[in] end - The ending time.
 * @param[in] start - The starting time.
 *
 * @returns result - The difference between the start time and end time.
 *****************************************************************************/
struct timeval timeval_subtract(struct timeval start, struct timeval end)
{
    struct timeval result;
    long int sec = 0; 
    long int usec = 0; 
    bool usec_ovflow = false;


    //We want positive numbers, so check if the start time is larger than the end time.
    if(start.tv_usec > end.tv_usec)
    {    
        //cout << "start time greater than end time" << endl;
        usec = (1000000 + end.tv_usec) - start.tv_usec;
        usec_ovflow = true;//Mark using flag so we can propagate up to seconds.
    }    
    else 
    {    
        usec = end.tv_usec - start.tv_usec;
    }    

    sec = end.tv_sec - start.tv_sec;

    if (usec_ovflow)
        sec = sec - 1; 

    result.tv_sec = sec;
    result.tv_usec = usec;

    return result;
}
