/*************************************************************************//**
 * @file
 *
 * @brief Monte Carlo estimation of pi via simulation similar to throwing
 * darts at a dart board.
 *
 * @mainpage Overview
 *
 * @section course_section Parallel Computing
 *
 * @author Andrew Pierson
 *
 * @date February 22, 2013
 *
 * @par Professor:
 *         Dr. Christer Karlsson
 *
 * @par Course:
 *         CSC 410 - M001 - 9:00am
 *
 * @par Location:
 *         McLaury
 *
 * @section program_section Program Information
 *
 * @details 
 * Suppose we toss darts randomly at a square dartboard, where the bullseye
 * is at the origin, and whose sides are 2 ft in length.  Suppose also that
 * there is a circle with radius 1 ft inscribed in the square dartboard. If the
 * points that are hit by the darts are uniformly distributed (and always
 * landing in the square), then the number of darts that hit inside the circle
 * should approximately satisfy the equation:
 *
 * number_in_circle/total_number_of_darts = pi/4
 *
 * Since the ratio of the area of the circle to the area of the square is π/4.
 * We can use this formula to estimate the value of π with a random number
 * generator.
 *
 * This is a so called Monte Carlo process, as it uses randomness to solve a 
 * problem.
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      g++ darts_omp.cpp -o darts_omp -fopenmp
 *
 * @par Usage:
 * @verbatim
 * $ darts.c number_of_darts number_of_threads
 * 
 * DESCRIPTION
 * Estimates the value of pi.
 * 
 * [number_of_darts]:
 * The number of darts that will be thrown at the dart board.
 * 
 * [number_of_threads]:
 * The number of threads that will be used to calculate pi.
 * 
 * @endverbatim
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 * @bug [FIXED] some_bug
 *
 *
 * @todo
 *
 * @par Modifications and Development Timeline:
 * @verbatim
 * Date          Modification
 * ------------  --------------------------------------------------------------
 * Feb 3, 2013   Program assigned
 * Feb 23, 2013  Started program and parsing arguments.
 * Feb 24, 2013  Wrote the calculating pi estimation.
 * Feb 25, 2013  Split the calculations between threads.
 * Feb 26, 2013  Added timings and prettied up the output. Also, ran it on
 *               different machines and ran timmings.
 * Feb 27, 2013  Tried debugging why it was slowing down when running on more
 *               cores.
 * Feb 28, 2013  Found that my problem was because of #defines.
 * @endverbatim
 *
 *****************************************************************************/
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <pthread.h>
#include <stdio.h>
#include <omp.h>
#include <sys/time.h>
#include <sys/types.h>



//-------------Globals------------
int thread_count;//Total number of threads.
long long int total_tosses;
long long int total_in_circle;//Used for aggrigating the final pi estimation.

//------Function Prototypes--------
void* Hello(void* rank);
void* throwDarts(void* rank);
struct timeval timeval_subtract(struct timeval start, struct timeval end);
long long int block_low(long long int rank,long long int threads,long long int tasks);
long long int block_high(long long int rank,long long int threads,long long int tasks);  
long long int block_size(long long int rank,long long int threads,long long int tasks);

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Fires up all the threads, waits for them to finish as
 * they calculate their section of pi then write the results to total_in_circle.
 * After they are finished the result and total time is printed to the console.
 *
 * @param[in] argc - The number of arguments.
 * @param[in] argv - The arguments from the command line.
 *
 * @returns 0 - The program terminated successfully.
 *****************************************************************************/
int main (int argc, char* argv[])
{
    //Variables for starting up threads.
    int re;//Result, used for checking errors.
    long long int thread;//Iterator for creating threads.
    long long int tosses;//The overall number of tosses.
    long long int dart_tosses;//The tosses for each thread.
    char response[256];//Response for the user input.
    long double pi_estimate;
    pthread_t*  threads;//The threads themselves.

    //Variables used for timing.
    struct timeval start, end;
    struct timeval duration;


    //If there are command line arguments use those.
    if (argc == 3)
    {
        thread_count = strtol(argv[1], NULL, 10);
        total_tosses = strtol(argv[2], NULL, 10);
    }

    else 
    {
        //Output info about the program to the user.
        std::cout << "----------------------------------------------\n";
        std::cout << "            DARTS, estimation of pi\n";
        std::cout << "----------------------------------------------\n";
        std::cout << "Monte Carlo estimtation of pi by throwing darts at a dart board.\n";
        std::cout << "Enter number of threads: ";
        std::cin >> response;
        thread_count = strtol(response, NULL, 10);
        std::cout << "Enter number of darts to throw: ";
        std::cin >> response;
        total_tosses = strtol(response, NULL, 10);
    }




    //Start timing.
    gettimeofday(&start, 0);

    //Initialize the total result and mutex.
    total_in_circle = 0.0;

    //Create each thread and tell it how many darts to throw.
#  pragma omp parallel num_threads(thread_count)
    throwDarts(0);



    //Stop the timer.
    gettimeofday(&end, 0);


    //Print out the times.
    duration = timeval_subtract(start, end);
    printf("Time: %ld.%06ld\n" ,duration.tv_sec, duration.tv_usec);

    //Calculate and print out the final pi estimation.
    pi_estimate = ((4.0*total_in_circle)/total_tosses);
    std::cout << "threads pi estimate: " << std::fixed << std::setprecision(10) << pi_estimate << std::endl;


    return 0;
}

/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: Subtracts two timeval structures, results in the 
 * time difference between the two. timeval is broken
 * up into two parts; seconds and micro seconds.
 *
 *
 * @param[in] end - The ending time.
 * @param[in] start - The starting time.
 *
 * @returns result - The difference between the start time and end time.
 *****************************************************************************/
struct timeval timeval_subtract(struct timeval start, struct timeval end)
{
    struct timeval result;
    long int sec = 0; 
    long int usec = 0; 
    bool usec_ovflow = false;


    //We want positive numbers, so check if the start time is larger than the end time.
    if(start.tv_usec > end.tv_usec)
    {    
        //cout << "start time greater than end time" << endl;
        usec = (1000000 + end.tv_usec) - start.tv_usec;
        usec_ovflow = true;//Mark using flag so we can propagate up to seconds.
    }    
    else 
    {    
        usec = end.tv_usec - start.tv_usec;
    }    

    sec = end.tv_sec - start.tv_sec;

    if (usec_ovflow)
        sec = sec - 1; 

    result.tv_sec = sec;
    result.tv_usec = usec;

    return result;
}





/**************************************************************************//**
 * @author Andrew Pierson
 *
 * @par Description: This function is passed to each thread. They will
 * throw darts and calculate a small portion of the value of pi and add it
 * to the main global pi estimation. This way pi does not need to be 
 * averaged in the main thread.
 *
 * @param[in] tosses - The number of tosses this thread will do to calculate
 * its portion of pi.
 *
 * @returns NULL - Indicates that it is done.
 *****************************************************************************/
void* throwDarts(void* rank)
{
    long long int my_rank = (long long int) rank;
    double x = 0;//X location of dart toss.
    double y = 0;//Y location of dart toss.
    double distFromCenter;//Distance the dart is from the center.
    long long int dartsOnTarget = 0;//Darts that ended up in circle.
    long long int toss = 0;//Iterator to keep track of tosses.
    long long int myTosses;
    struct drand48_data drand_buff;

    //Seed the random number generator.
    srand48_r(time(NULL)+ my_rank, &drand_buff);


    //Calculate the # of darts to throw for this particular thread.
    myTosses = block_size(my_rank, thread_count, total_tosses); 


    //Toss all darts and keep track of the ones that land inside the circle.
    for (toss = 0; toss < myTosses; toss++)
    {
        drand48_r(&drand_buff, &x);
        drand48_r(&drand_buff, &y);

        
        x = (2.0*x) - 1;//Random number between -1 and 1
        y = (2.0*y) - 1;


        distFromCenter = (x*x + y*y);
        if (distFromCenter <= 1)
        {
            dartsOnTarget ++;
        }
    }


    // Critical section
#       pragma omp parallel reduction(+:total_in_circle)
        total_in_circle += dartsOnTarget; 

    return NULL;
}



long long int block_low(long long int rank,long long int threads,long long int tasks){
    return ((rank)*(tasks)/(threads));
}
long long int block_high(long long int rank,long long int threads,long long int tasks){  
   return (block_low((rank)+1,threads,tasks)-1);
}
long long int block_size(long long int rank,long long int threads,long long int tasks){
    return (block_high(rank,threads,tasks)-block_low(rank,threads,tasks)+1);
}
