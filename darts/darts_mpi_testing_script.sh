 #! /bin/bash

 #The output of darts_mpi may need to be tweeked
 #so that only the time is displayd
 #for this script to run well and look nice.
 
 #Clear the outputfile
 ''>output.csv
 file='../open.hosts'
 #for each number of processors
 for i in 1 8 16 32 64
 do
     #and for every problem size
     echo "number of processors: $i" | tee -a output.csv
     for j in 500000000 1000000000 2000000000
     do
         #run the program with the number of processors using the
         #variable j as input to the program
         #tee will output the values to screen and to an output file
         echo -n "$j, " | tee -a output.csv
         mpiexec -np $i -hostfile $file ./darts_mpi $j | tee -a output.csv
     done
 done
