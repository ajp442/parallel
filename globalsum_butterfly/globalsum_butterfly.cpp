/*************************************************************************//**
 * @file
 *
 * @brief computes a global sum and distributes the results to all processes
 * using a butterfly algorythm.
 *
 * @mainpage Overview
 *
 * @section course_section Parallel Computing
 *
 * @author Andrew Pierson
 *
 * @date April 03, 2013
 *
 * @par Professor:
 *         Dr. Christer Karlsson
 *
 * @par Course:
 *         CSC 410 - 9:00am
 *
 *
 * @section program_section Program Information
 *
 * @details Detailed_description_of_the_program 
 * Write an MPI program that
 * computes a global sum and distributes the results to all processes using a
 * butterfly algorithm (see pg 107).  First write your code for the special case
 * in which comm_sz (number of processes) is a power of two.  Then modify your
 * code so that it will be able to handle any number of processes.  Time your code
 * and compare the results from your code against the results for MPI_Allreduce(),
 * using the same number of processes.
 *
 * For my implementation, since the purpose of this program is more a proof of concept
 * than solving a problem; The data that each node starts with is the same as it's
 * rank. This makes it easy to debug.
 *
 * I implemented a home-made method for handling any number of processes. My method
 * consists of finding the extra nodes (extra nodes are the nodes above the nearest
 * lowest power of two). These nodes will send their values to the lowest n nodes
 * where n is the number of extra nodes. A normal butterfly sum is performed.
 * At the end the first n nodes send their results back to the extra nodes.
 * This leaves all nodes with the global sum.
 *
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *    mpic++ -g -Wall -lm -o globalsum_butterfly globalsum_butterfly.cpp
 *
 * @par Usage:
 *    mpiexec -np <number of processes> ./globalsum_butterfly
 *    mpiexec -np <number of processes> -hostfile <list of hosts> ./globalsum_butterfly
 *    On my system you need to use:
 *    /usr/lib64/openmpi/bin/mpirun -np 9 -mca btl ^openib ./globalsum_butterfly
 *
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 *
 *
 * @todo
 *
 * @par Modifications and Development Timeline:
 * @verbatim
 * Date          Modification
 * ------------  --------------------------------------------------------------
 * Mar 01, 2013   Program assigned
 * Apr 02, 2013   Started working on program.
 * Apr 03, 2013   Added comments and did touch up work.
 * Apr 03, 2013   Program due
 * May 02, 2013   Just reviewing for final, thought I would try to get this
 *                working for review.
 * @endverbatim
 *
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <sys/time.h>
#include <cmath>
#include <unistd.h>


bool isPowerOfTwo(int x);
int distToPowerOfTwo(int x);

int main(int argc, char *argv[]) {
    int thread_count;//Number of processes.
    int rank;//Rank (ID) of each thread.
    int extraNodes;//The number of extra nodes beyond the nearest lower power of two.
    int num;//The number that each node contains (it's local sum)
    int temp_num;//A teomporary stroage variable.
    int steps;//The total number of steps. This will be log2(thread_count).
    int twoToTheStep;//2^(step)
    int passLeft;//Used as a boolean te determine if a particular thead should be passing to the left.
    int buddy;//The rank of the thread to sent to.

    //Timing variables for custom version.
    double start_cust;
    double end_cust;
    double duration_cust;
    //Timing variables for mpi version
    double start_mpi;
    double end_mpi;
    double duration_mpi;


    //MPI_Init(&argc, &argv);
    MPI_Init(NULL, NULL);//Initiate.
    MPI_Comm_size(MPI_COMM_WORLD, &thread_count);//Get number of processes.
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);//Get the rank of the process.

    MPI_Barrier(MPI_COMM_WORLD);
    //Start custom timing.
    if (rank == 0) {
        start_cust = MPI_Wtime();
    }


    steps = log2(thread_count);
    num = rank;
    extraNodes = distToPowerOfTwo(thread_count);

    //Hav the extra nodes send thier numbers to the first few regular nodes. 
    for (int i = 0; i < extraNodes; i++)
    {
        buddy = thread_count - 1 - i;
        if (rank == i)
        {
            MPI_Recv(&temp_num, 1, MPI_INTEGER, buddy, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            num = num + temp_num;
        }
        else if (rank == buddy)
        {
            MPI_Send(&num, 1, MPI_INTEGER, i, 0, MPI_COMM_WORLD);
        }
    }

    //Do a starndard butterfly algorythm with all the nodes that fall within the power of 2.
    for (int step = 0; step < steps; step++)
    {

        twoToTheStep = (int)pow(2, step);
        passLeft = rank >> step;
        passLeft = passLeft & 1;

        //buddy = rank+(2^(step))*(-1)^((step>>rank)&1)
        if (passLeft)
        {
            buddy = rank - twoToTheStep;
            if (buddy >= 0)
            {
                MPI_Send(&num, 1, MPI_INTEGER, buddy, 0, MPI_COMM_WORLD);
                MPI_Recv(&temp_num, 1, MPI_INTEGER, buddy, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
        }
        else 
        {
            buddy = rank + twoToTheStep;
            if (buddy < (thread_count)) {
                MPI_Send(&num, 1, MPI_INTEGER, buddy, 0, MPI_COMM_WORLD);
                MPI_Recv(&temp_num, 1, MPI_INTEGER, buddy, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
        }

        num = num + temp_num;
    }


    //Have the first few nodes send their results to the extra nodes.
    for (int i = 0; i < extraNodes; i++)
    {
        buddy = thread_count - 1 - i;
        if (rank == i)
        {
            MPI_Send(&num, 1, MPI_INTEGER, buddy, 0, MPI_COMM_WORLD);
        }
        else if (rank == buddy)
        {
            MPI_Recv(&num, 1, MPI_INTEGER, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
    }
    //End custom timing
    if (rank == 0) {
        end_cust = MPI_Wtime();
    }

#if DEBUG
    if (rank != 0)
    {
        MPI_Send(&num, 1, MPI_INTEGER, 0, 0, MPI_COMM_WORLD);
    }
    else 
    {
        printf("rank %d has %d\n", rank, num);
        for (int i = 1; i<thread_count; i++)
        {
            MPI_Recv(&num, 1, MPI_INTEGER, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            printf("rank %d has %d\n", i, num);
        }
    }
#endif

    //Reset all the numbers.
    num = rank;

    //Start mpi timing.
    usleep(100);
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        start_mpi = MPI_Wtime();
    }
    MPI_Allreduce(&num, &num, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD);
    //End mpi timing
    if (rank == 0) {
        end_mpi = MPI_Wtime();
    }



    if (rank == 0)
    {
        duration_cust = end_cust - start_cust;
        duration_mpi = end_mpi - start_mpi;
        //Output our findings.
        printf("Duration custom: %lf\n" , duration_cust);
        printf("Duration mpi:    %lf\n" , duration_mpi);
    }

    MPI_Finalize();
    return 0;
}

bool isPowerOfTwo(int x)
{
    return (x != 0) && ((x & (x - 1)) == 0);
}

int distToPowerOfTwo(int x)
{
    int i = 0;
    while (!isPowerOfTwo(x))
    {
        x--;
        i++;
    }
    return i;
}
